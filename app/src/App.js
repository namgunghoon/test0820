import React from 'react';
import './App.css';
import { getAnswer1 } from './Test1';
import { getAnswer2 } from './Test2';
import { getAnswer3 } from './Test3';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      answer1_1: getAnswer1()[0],
      answer1_2: getAnswer1()[1],
      answer1_3: getAnswer1()[2],
      answer1_4: getAnswer1()[3],
      answer1_5: getAnswer1()[4],
      answer2: '',
      answer3: '',
      participant: '',
      completion: '',
      s: ''
    }
  }

  handleAnswer2 = () => {
    this.setState({answer2: getAnswer2(this.state.participant, this.state.completion)});
  } 

  handleAnswer3 = () => {
    this.setState({answer3: getAnswer3(this.state.s)});
  } 

  setCompletion = e => {
    this.setState({completion: e.target.value});
  }

  setS = e => {
    this.setState({s: e.target.value});
  }
  
  setParticipant = e => {
    this.setState({participant: e.target.value});
  }

  render(){
    return (
      <div className="App">
        <header className="App-header">
          <b1>지원자의 입력 답변</b1>
          <br/>
          <b1>TEST1</b1>
          <b1>답변1-1 : {this.state.answer1_1}</b1>
          <b1>답변1-2 : {this.state.answer1_2}</b1>
          <b1>답변1-3 : {this.state.answer1_3}</b1>
          <b1>답변1-4 : {this.state.answer1_4}</b1>
          <b1>답변1-5 : {this.state.answer1_5}</b1>
          <br/>
          <b1>TEST2</b1>
          <b1><input type='text' placeholder='participant (e.g. a,b,c..)' onChange={this.setParticipant} name='participant'></input></b1>
          <b1><input type='text' placeholder='completion (e.g. a,b,c..)' onChange={this.setCompletion} name='completion'></input></b1>
          <b1><button onClick={this.handleAnswer2}>입력</button></b1>
          <b1>답변2 : {this.state.answer2}</b1>
          <br/>
          <b1>TEST3</b1>
          <b1><input type='text' placeholder='s (e.g. 1 2 3 4)' onChange={this.setS} name='completion'></input></b1>
          <b1><button onClick={this.handleAnswer3}>입력</button></b1>
          <b1>답변3 : {this.state.answer3}</b1>
        </header>
      </div>
    );
  }
}