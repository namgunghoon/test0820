export function getAnswer1() {
    return [
        "var:재선언가능, 함수레벨 let:재선언불가능, 재할당가능, 블록레벨 const:재선언불가, 재할당불가, 블록레벨",
        "mutable:메모리 영역에서 변경 가능 immutable:메모리 영역에서 변경 불가능",
        "callback:복잡성 증가, 가독성 떨어짐(callback hell), 예외처리 힘듬,   promise:체이닝을 통한 유지보수성 증가, 비동기작업 추가 용이",
        "this의 바인딩시 call은 인자를 따로 넣지만 apply는 두번째 인자부터 모두 배열에 넣어야 함:",
        "null:빈값이 할당 undefined:할당된 값이 없음 undeclared:변수조차 선언되지 않음"
    ];
}