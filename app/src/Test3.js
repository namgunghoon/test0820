export function getAnswer3(splArr) {
    var arr = splArr.split(' ');
    return Math.min(...arr) + ' ' + Math.max(...arr);
}