export function getAnswer2(participant, completion) {
    participant = participant.replace(" ","").split(",");
    completion = completion.replace(" ","").split(",");
    participant.sort()
    completion.sort()

    return participant.find((e, i) => {
       if(e !== completion[i]){
           return e
       }else {
           return null;
       } 
    });
}